app.controller('AppCtrl', ['$scope', '$mdDialog', '$http', '$location', function($scope, $mdDialog, $http, $location) {
    $http({
        url: '/api/questions/list',
        method: 'POST',
        headers: {
            'Content-Type': "application/json"
        },
        data: {
            test: 'test'
        }
    }).then(function successCallback(response) {
        if ( ! response.data.success)
            alert($mdDialog, 'Error 110010100', 'Something wrong happened', 'Got it!');

        $scope.questions = response.data;

    }, function errorCallback() {
        alert($mdDialog, 'Error 110010100', 'Something wrong happened', 'Got it!');
    });

    $scope.goBack = function(ev) {
        $location.path("/");
    };

    $scope.showDialog = function(ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'templates/_questionDialog.tpl.html',
            targetEvent: ev,
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
            .then(function(newQuestion) {
                $http({
                    url: '/api/questions/create',
                    method: 'POST',
                    data: newQuestion
                }).then(function successCallback(response) {
                    newQuestion.id = response.data.data.toString();
                    newQuestion.date = getDate();
                    newQuestion.answers = "0";

                    if (response.data.success)
                        $scope.questions.data.push(newQuestion);

                    console.log($scope.questions.data);

                    $mdDialog.cancel();
                });
            });
    };

    function DialogController($scope, $mdDialog) {
        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.setQuestion = function() {
            if ($scope.addQuestion.aName.$valid && $scope.addQuestion.aTitle.$valid && $scope.addQuestion.aDescription.$valid)
                $mdDialog.hide($scope.newQuestion);
        };
    }

    function alert($mdDialog, _title, _content, _ok) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.body))
                .clickOutsideToClose(true)
                .title(_title)
                .textContent(_content)
                .ok(_ok)
        );
    }

    function getDate() {
        var date = new Date(),
            Y = date.getFullYear(),
            M = (date.getMonth() < 9 ? "0" : "") + (date.getMonth() + 1).toString(),
            D = (date.getDate() < 10 ? "0" : "") + date.getDate(),
            h = (date.getHours() < 10 ? "0" : "") + date.getHours(),
            m = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes(),
            s = (date.getSeconds() < 10 ? "0" : "") + date.getSeconds();

        return Y + "-" + M + "-" + D + " " +  h + ":" + m + ":" + s;
    }
}]);