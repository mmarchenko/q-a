app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl : 'templates/home.tpl.html'
        })
        .when('/question/:id', {
            templateUrl : 'templates/question.tpl.html',
            controller  : 'questionCtrl'
        });
});

app.filter('dateToISO', function() {
    return function(input) {
        return new Date(input).toISOString();
    };
});