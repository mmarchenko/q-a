var app = angular.module('qaApp', ['ngMaterial', 'ngRoute', 'ngMessages']);

@import "config.js";
@import "app.js";
@import "answers.js";