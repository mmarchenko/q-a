app.controller('questionCtrl', ['$scope', '$routeParams', '$location', '$filter', '$http', function($scope, $routeParams, $location, $filter, $http) {
    var qid = $routeParams.id,
        index;

    $scope.question = $filter('filter')($scope.questions.data, function (_v, _i) {index = _i; return _v.id === qid;})[0];

    if (typeof $scope.question == 'undefined')
        $location.path("/");

    $http({
        url: '/api/answers/list',
        method: 'POST',
        data: {
            qid: qid
        }
    }).then(function successCallback(response) {
        $scope.answers = response.data;
    });

    $scope.answer = function() {
        if ($scope.addAnswer.aName.$valid && $scope.addAnswer.aText.$valid ) {
            $scope.newAnswer.qid = qid;
            $scope.newAnswer.date = new Date().toISOString();

            $http({
                url: '/api/answers/create',
                method: 'POST',
                data: $scope.newAnswer
            }).then(function successCallback(response) {
                if (response.data.success) {
                    $scope.answers.data.push($scope.newAnswer);
                    $scope.questions.data[index].answers = (parseInt($scope.questions.data[index].answers) + 1).toString();
                }
            });
        }
    };
}]);