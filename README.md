Q&A Service -- Test Task
===========================

REQUIREMENTS
------------

The minimum requirement is that your Web server supports:

- PHP 5.4+
- MySQL 5.5+
- Enabled AllowOverwrite
- Enabled ModRewrite


INSTALLATION
-------------------

First of all, you need create your database and restore database from dump
```
$ mysql -u [user_name] -p [user_password] -h [db_host]

mysql> create database [db_name];
Query OK, 1 row affected (0.00 sec)

mysql> use [db_name];
Database changed

mysql> source qna.sql;
Query OK,..
Query OK,..
..
```

or if you already have created database:
```
$ mysql -u [user_name] -p [user_password] -h [db_host] [db_name] < qna.sql
```

Then, replace database credentials in `/api/config/DB.php`
```php
private $_host = "127.0.0.1";   // Host
private $_username = "root";    // User name
private $_password = "";        // User password
private $_database = "qna";     // Database name
```