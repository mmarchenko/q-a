module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        import: {
            options: {},
            dist: {
                src: 'common/js/main.js',
                dest: 'js/global.min.js'
            }
        },

        sass: {
            development: {
                options: {
                    style: 'compressed',
                    spawn: false
                },
                files: {
                    'style.css': 'common/sass/main.scss'
                }
            }
        },

        watch: {
            options: {
                //livereload: true
            },
            scripts: {
                files: 'common/js/**/*.js',
                tasks: ['import'],
                options: {
                    spawn: false
                }
            },
            css: {
                files: 'common/sass/**/*.scss',
                tasks: ['sass'],
                options: {
                    spawn: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-import');

    grunt.registerTask('default', ['import', 'sass', 'watch']);

};