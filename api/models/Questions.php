<?php

class QuestionsModel
{
    private $_db;
     
    public function __construct()
    {
        $this->_db = DB::getInstance();
    }

    public function create($userId, $title, $description)
    {
        $title = $this->_db->escape($title);
        $description = $this->_db->escape($description);

        $sql = "INSERT INTO questions (user_id, title, description) 
                VALUES ({$userId} ,'{$title}', '{$description}')";

        $this->_db->query($sql);

        return $this->_db->getId();
    }

    public function getList()
    {
        $sql = "SELECT questions.id, questions.title, questions.description, questions.date, users.name, (
                    SELECT COUNT(answers.id) 
                    FROM answers 
                    WHERE answers.question_id = questions.id) AS answers
                FROM questions
                INNER JOIN users 
                ON users.id = questions.user_id";

        $this->_db->query($sql);

        return $this->_db->allAssoc();
    }
}