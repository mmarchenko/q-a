<?php

class AnswersModel
{
    private $_db;
    
    public function __construct()
    {
        $this->_db = DB::getInstance();
    }

    public function create($qid, $userId, $text)
    {
        $sql = "INSERT INTO answers (question_id, user_id, text)
                VALUES ('{$qid}', '{$userId}', '{$text}')";

        $this->_db->query($sql);

        return $this->_db->getId();
    }

    public function getList($qid)
    {
        $qid = $this->_db->escape($qid);

        $sql = "SELECT answers.text, answers.date, users.name
                FROM answers
                INNER JOIN users 
                ON users.id = answers.user_id
                WHERE question_id = '{$qid}'";

        $this->_db->query($sql);

        return $this->_db->allAssoc();
    }
}