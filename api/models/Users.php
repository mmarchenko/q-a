<?php

class UsersModel
{
    private $_db;
     
    public function __construct()
    {
        $this->_db = DB::getInstance();
    }

    public function create($name)
    {
        $name = $this->_db->escape($name);
        $ip = $_SERVER['REMOTE_ADDR'];

        $sql = "INSERT INTO users (name, ip)
                VALUES ('{$name}', '{$ip}')";

        $this->_db->query($sql);

        return $this->_db->getId();
    }
}