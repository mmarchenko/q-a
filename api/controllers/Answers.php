<?php

class Answers
{
    private $_params;
     
    public function __construct($params)
    {
        $this->_params = $params;
    }

    public function createAction()
    {
        $qid = $this->_params->qid;
        $userName = $this->_params->name;
        $text = $this->_params->text;

        if ($qid && $userName && $text) {
            $userId = (new UsersModel())->create($userName);

            return (new AnswersModel())->create($qid, $userId, $text);
        }

        return false;
    }

    public function listAction() 
    {
        $qid = $this->_params->qid;

        if ($qid) {
            return (new AnswersModel())->getList($qid);
        }

        return false;
    }
}