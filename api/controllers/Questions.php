<?php

class Questions
{
    private $_params;
     
    public function __construct($params)
    {
        $this->_params = $params;
    }

    public function createAction()
    {
        $userName = $this->_params->name;
        $title = $this->_params->title;
        $description = $this->_params->description;

        if ($userName && $title && $description) {
            $userId = (new UsersModel())->create($userName);

            return (new QuestionsModel())->create($userId, $title, $description);
        }

        return false;
    }

    public function listAction() 
    {
        return (new QuestionsModel())->getList();
    }
}