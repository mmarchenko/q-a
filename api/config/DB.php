<?php

class DB
{
	private $_connection;
	private $_result;
	private static $_instance;

	private $_host = "127.0.0.1";
	private $_username = "root";
	private $_password = "";
	private $_database = "qna";

	/**
	 * Constructor for DB singleton class.
	 */
	private function __construct() 
	{
		$this->_connection = new mysqli($this->_host, $this->_username, 
			$this->_password, $this->_database);

		if (mysqli_connect_error())
			trigger_error("Failed to conencto to MySQL: ". mysql_connect_error(), 
				E_USER_ERROR);
	}

	/**
	 * @return mysqli
	 */
	public function getConnection()
	{
		return $this->_connection;
	}

	/**
	 * @param $sql string
	 * @return bool|mysqli_result
	 */
	public function query($sql)
	{
		$this->_result = $this->_connection->query($sql);

		return $this->_result;
	}

	/**
	 * Get query execution status.
	 *
	 * @return bool	Return true if INSERT/DELETE/DROP/.. is success
	 */
	public function success()
	{
		return $this->_result === TRUE;
	}

	/**
	 * Return index(column with attribute AUTO_INCREMENT) of last insert query
	 *
	 * @return bool|mixed
	 */
	public function getId()
	{
		if ($this->success())
			return $this->_connection->insert_id;

		return false;
	}

	/**
	 * Escape string before add to query
	 *
	 * @param $value
	 * @return string
	 */
	public function escape($value)
	{
		$value = $this->_connection->real_escape_string($value);

		return $value;
	}

	/**
	 * Get associative array from DB
	 *
	 * @return array
	 */
	public function allAssoc()
	{
		$allAssoc = [];
        while($row = $this->_result->fetch_assoc())
            $allAssoc[] = $row;

		return $allAssoc;
	}

	public static function getInstance()
	{
		if ( ! self::$_instance)
			self::$_instance = new self();

		return self::$_instance;
	}

	private function __clone() { }
}