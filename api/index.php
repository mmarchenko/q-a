<?php
include_once "config/DB.php";
include_once "models/Users.php";
include_once "models/Questions.php";
include_once "models/Answers.php";

try {
    $request = $_REQUEST;

    $controller = ucfirst(strtolower($request['controller']));
    $action = strtolower($request['action']).'Action';
 
    if (file_exists("controllers/{$controller}.php"))
        include_once "controllers/{$controller}.php";
    else
        throw new Exception('Controller is invalid.');

    $params = json_decode(file_get_contents('php://input'), true);
    $controller = new $controller((object)$params);

    if (method_exists($controller, $action) === false)
        throw new Exception('Action is invalid.');

    $result['data'] = $controller->$action();
    $result['success'] = $result['data'] ? true : false;
     
} catch( Exception $e ) {
    $result = array();
    $result['success'] = false;
    $result['errormsg'] = $e->getMessage();
}

header('Content-Type: application/json');
echo json_encode($result);
exit();